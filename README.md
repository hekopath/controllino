# Controllino hotspot owners' community knowledge base

## Quick actions

| [Connect with community](#community) | [Flashing alternative firmware](#software) | [Problems and solutions](#problems) |
| :---:                                | :---:                                      | :---:                               |

## News

- 2023-02-18 Community knowledge base has been started with the goal of collecting and summarizing useful information

## Community

**Discord server:** https://discord.io/controllino

## Hardware

| [Controllino v1](/Hardware/Controllino-v1/README.md) | [Controllino v2](/Hardware/Controllino-v2/README.md) | [Controllino Pro]((/Hardware/Controllino-Pro/README.md)) |
| :---:          | :---:          | :---:           |
| [![Example footage of Controllino v1](/Hardware/Controllino-v1/.img/controllino-v1_1.jpg "Controllino v1")](/Hardware/Controllino-v1/README.md) | [![Example footage of Controllino v2](/Hardware/Controllino-v2/.img/controllino-v2_1.jpg "Controllino v2")](/Hardware/Controllino-v2/README.md) | [![Example footage of Controllino Pro](/Hardware/Controllino-Pro/.img/controllino-pro_1.jpg "Controllino Pro")](/Hardware/Controllino-Pro/README.md) |

## Software

### Controllino original firmware

**Latest miner version:** 2023.02.07.0

**Latest dashboard version:** 1.4.3

### Nebra alternative firmware

**Latest miner version:** #TODO

**Latest dashboard version:** #TODO

**Flashing tutorials:**

- [Nebra official](https://support.nebra.com/support/solutions/articles/24000078655-getting-started-with-controllino "Nebra official")
- Uros alternative #TODO @Uros

[More detailed information](./Software/Nebra-firmware/README.md)

### Community alternative firmware

\#TODO it would be better to name it after it's inventor

## Problems
### Open Problems
- Onboarding when maker's wallet is low on DC #TODO new page
- Onboarding after migration to Solana #TODO new page
- Finding a maker to take over Controllino hotspots #TODO new page
- Keeping hotspots operational if Controllino will shut down completely #TODO new page

### Resolved Problems

## Additional Resources

Official Controllino (since 2022-11 renamed to Conelcom) web

* main page: https://www.conelcomhotspot.com
* support portal: https://controllinohotspot.zendesk.com
* Discord server: https://discord.gg/HCp6wBuhwF
