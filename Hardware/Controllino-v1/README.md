# Controllino v1

The first Controllino hotspot which is the most widespread one.

## Internal parts

Raspberry Pi 4

- 64 bit Quad Core ARM v8 Cortex-A72 @ 1,5 GHz
- 8GB LPDDR4 RAM

Micro SD card

- 32 GB SD card
- Hints for selecting replacement Micro SD card: [Anandtech article](https://www.anandtech.com/show/15639/sandisk-launches-max-endurance-microsd-cards-3-to-15-years-warranty)

Daughter board

- EMB-RasPI-130x-Cape [Embit Product Page]( http://www.embit.eu/products/oem-gateways/accessories/emb-raspi-lr130x-cape/)
                    [Datasheet PDF]( http://www.embit.eu/wp-content/uploads/Datasheet-EMB-RasPI-130x-Cape.pdf)

LoRa card

- SX1302 LoRa Module EMB-LR1302-mPCIe [Embit Product Page](http://www.embit.eu/products/oem-gateways/emb-lr1302-mpci-e-prova/)
- custom version with proprietary mPCIe (SPI / I2C / GPIOs) matching EMB-RasPI-130x-Cape
- Version with one u.fl connector

RF pigtail

\#TODO

Power connector

- 5V/3A Power Supply with 5.5/2.1 mm DC Connector

Case and mechanical parts

- SMA Female connector 
  - ![SMA female connector](/Hardware/Controllino-v1/.img/SMA_female.png "SMA female")
- Gigabit Ethernet

Antenna

- 2.2dBi LoRa 868MHz Antenna, SMA Male connector



