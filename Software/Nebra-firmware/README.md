# Nebra alternative firmware

Nebra has ported their hotspot firmware on Controllino (v1 and Pro) hotspots.

## Flashing
[Nebra official tutorial][def]

[def]: https://support.nebra.com/support/solutions/articles/24000078655-getting-started-with-controllino "Nebra official tutorial"

## Additional information and actions
 - [enabling SSH access](./ssh-access/README.md)
 - [enabling WiFi](./enable-wifi/README.md)